# Installation Guide

The following guide will assume you have the necessary pre-requisites listed on the README.md already available.


## Steps for Install:

1. Before Running this script **REVIEW** the code and ensure you are fine with the changes being made to your operating system. 


2. Enter into your terminal `git clone https://gitlab.com/palworld1/palworld-linux-scripts.git`.

    ![step1](./photos/step1.png)

3. Change to the scripts directory `cd palworld-linux-scripts/scripts`.

    ![step2](./photos/step2.png)

4. In the scripts directory edit `config.sh`. Enter `nano config.sh`.

    ![step3](./photos/step3.png)
    

5. Within the script change the `RCON_PASSWORD` to what you want your server admin password to be.

    ![step4](./photos/step4.png)

    - When you have finished editing this press `ctrl + x` then `y` then `enter` and it will save and close the file.


<div>&nbsp;</div>

6. Next edit the `initial-start.sh`. Enter `nano initial-start.sh`.

    ![step5](./photos/step5.png)

<div>&nbsp;</div>

7. Find the section for variable controls and supply your own
    - This var on line 49: ADMIN_PASS="asfkl340a9s8SKFJHF"
    - This var on line 50: SERVER_DESC="My PalWorld Server!"
    - This var on line 51: SERVER_NAME="PalWorld-Fun"
    - This var on line 52: SERVER_PASS="sczSKD834f982Kf9"
    - This var on line 53: PORT=8211

    ![step6](./photos/step6.png)

    - When you have finished editing this press `ctrl + x` then `y` then `enter` and it will save and close the file.

<div>&nbsp;</div>

8. Now that we've done some pre-configuration and you have reviewed the other scripts allow all scripts in the current directory to be executable. Enter `chmod +x *.sh`.

    ![step7](./photos/step7.png)

<div>&nbsp;</div>

**WARNING DO NOT DO STEP 9-15 IF YOU ALREADY HAVE DATA IT WILL MESS WITH PLAYER DATA DUE TO STEAM SDK**  

<div>&nbsp;</div>

9. Its time to start the install. Enter `./setup.sh`

    ![step8](./photos/step8.png)

<div>&nbsp;</div>

10. First we set the timezone of your server (this is mostly for cron jobs). Enter a number that corresponds with the timezone you wish to use.

    ![step9](./photos/step9.png)

<div>&nbsp;</div>

11. Next It will pull down the ARRCON package mentioned in the README.md. This is what is used to connect via RCON to your server.

    ![step10](./photos/step10.png)

<div>&nbsp;</div>

12. Next the multiverse settings will update and install for SteamCMD.
    - Follow the onscreen prompts to accept and continue through the install. You may need to use tab and enter to navigate.

        ![step11](./photos/step11.png)

    12a. You may get something like this. Press `Enter`.

    ![step11a](./photos/step11a.png)

    12b. Arrow pad down to `I AGREE` and press `Enter`.

    ![step11b](./photos/step11b.png)

    12c. Finally it may ask during the update and install to restart services. This is normal. Simply arrow down to OK and press `Enter`.

    ![step11b](./photos/step11c.png)

<div>&nbsp;</div>

13. SteamCMD will install itself + the needed redistributables.

    ![step12](./photos/step12.png)

<div>&nbsp;</div>

14. Finally Palworld will install itself.

    ![step13](./photos/step13.png)

<div>&nbsp;</div>

15. Now that the server files are installed we need to generate some files for our server. Enter `./initial-start`.

    ![step14](./photos/step14.png)

<div>&nbsp;</div>

16. The server wil start and stop to generate our directories. Then we will stamp in the values you edited in the script into a PalWorldSettings.ini file in the correct place.

    ![step15](./photos/step15.png)

    16a. This script will tell you where the file is.

    ![step15a](./photos/step15a.png)

    16b. You can edit this file by doing `nano That_Path`.

    ![step15b](./photos/step15b.png)

    16c. Feel free to make any edits you want.

    ![step15c](./photos/step15c.png)   

    16d. You can confirm your password and other settings were properly entered.

    ![step15d](./photos/step15d.png)   

<div>&nbsp;</div>

17. Run `./server-check.sh` to start your server so we can get the world and player files.

    ![step16](./photos/step16.png)  

<div>&nbsp;</div>

18. Launch Palworld and go to Dedicated Server settings. On the bottom for direct connect check the `Enter Password`.

    ![step17](./photos/step17.png)  

<div>&nbsp;</div>

19. Enter your IP and Port on the right then click connect and enter your password.

    ![step17a](./photos/step17a.png)  

<div>&nbsp;</div>

20. Enter the password you made for your server.

    ![step18](./photos/step18.png)  

<div>&nbsp;</div>

21. Make your character.

    ![step19](./photos/step19.png)  

<div>&nbsp;</div>

22. Press escape to see your server name and description as well as basic metrics.

    ![step20](./photos/step20.png)  

<div>&nbsp;</div>

23. After you run around for a few min do `/AdminPassword YourPassword`.

    ![step21](./photos/step21.png)  

    23a. You should get a return for `You Are Now Admin`.

    ![step21a](./photos/step21a.png)  

    23b. You can enter commands like `/Save` to save the server.

    ![step21b](./photos/step21b.png)  

    23c. You can enter commands like `/Shutdown` to gracefully / safely stop the server.

    ![step21c](./photos/step21c.png)  

    23d. When the server closes you will be sent back to the main menu.

    ![step21d](./photos/step21d.png)  


    ![step21e](./photos/step21e.png)  

<div>&nbsp;</div>

24. Now we can trigger the backup script with `./backup-sh`.

    ![step22](./photos/step22.png)  

<div>&nbsp;</div>

25. The completion will make a new directory in your home directory called `PalWorld-Backups`.

    This backup is called `palworld-backup-Month-Day-Year-Hour-Min-AM/PM.tar` and tars up everything in the `/Pal` directory.

    ![step23](./photos/step23.png)  

<div>&nbsp;</div>

26. You can change to your backups directory and view your backups. Do `cd ~/PalWorld-Backups`.

    ![step24](./photos/step24.png)  

<div>&nbsp;</div>

27. Go ahead and go back to the scripts directory. `cd ~/palworld-linux-scripts/scripts/`.

    ![step25](./photos/step25.png)  

<div>&nbsp;</div>

28. Now execute `auto-cron.sh` Do `./auto-cron.sh`.

    ![step26](./photos/step26.png)  

<div>&nbsp;</div>

29. You can view your crontab with `crontab -l`.

    ![step27](./photos/step27.png)  

<div>&nbsp;</div>

30. Now we wait for intervals of 0 or 5 for the `./server-check` script to automatically start the server.

    ![step28](./photos/step28.png)  

    30a. Here's a 5 min later trigger

    ![step28a](./photos/step28a.png)  

<div>&nbsp;</div>

31. You can view the screen at any time by doing `screen -r palworld`.

    ![step29](./photos/step29.png)  


    31a. This screen shows you the actual `PalServer.sh` executable run from SteamCMD.

    ![step29a](./photos/step29a.png)  

    31b. To detach from this screen enter `cntrl + a + d`. Or Hold `cntrl` and then press `a` then `d`.

    ![step29b](./photos/step29b.png)  

<div>&nbsp;</div>

32. To manually trigger an update you can run the `./update.sh`. Via cron this will run at 4am every day.

    ![step30](./photos/step30.png)  


    32a. The update script will take over your current console and RCON into the server to initiate a save and shutdown.

    ![step30a](./photos/step30a.png)  

    32b. After the shutdown it will open a new screen that will update the server.

    ![step30b](./photos/step30b.png)  

<div>&nbsp;</div>

33. When the update is complete it will return the console to you. You can check to see if any screens are running by doing a `screen -list`.

    ![step31](./photos/step31.png)  


    33a. On the next 0 or 5 interval the server will start again.

    ![step31a](./photos/step31a.png)  

<div>&nbsp;</div>

34. Congratulations this concludes PalWorld server setup with automatic backups, restarts, and updating!


