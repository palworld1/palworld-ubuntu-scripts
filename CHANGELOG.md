# Changelog

## [Version 0.1.0] - 2-4-2024

### Added
- Added a new install guide with pictures!
- Added config.sh for sharing certain variables 

### Changed
- Updated `auto-cron.sh` to include update
- Updated README.md roadmap + accomplished objectives
- Updated `server-check.sh` to now check for system memory usage
- Altered the save and broadcast notifications in `sever-reboot.sh`
- Altered README.md to be more streamlined. 
- Altered `backup.sh` to cleanup files that are older than 3 days in the PalWorld-Backups directory

### Fixed
- Fixed various issues with path identification of where the scripts were being run from
- Fixed `server-reboot.sh` not properly rebooting the server

### Tests
- Tested on Ubuntu 22.04 Jammy
- Tested on Centos Stream 9


## [Beta ] - 1-27-24 Early AM

### Changed
- updated `auto-cron.sh` to source from launch directory. (this will help with future updates)
- Updated README.md roadmap + accomplished objectives

### Fixed
- Fixed the homedir calculation to not be the current working path which caused `setup.sh` failures.

### Tests
- Tested on Ubuntu 22.04
- Tested on Centos Stream 9

## [Beta ] - 1-26-24 
### Added
- Centos and Ubuntu are now supported Linux OS
- update.sh script to update your server (currently manual)
- Added a CHANGELOG.MD 😎

### Changed
- setup.sh now will grab all necessary dependencies for all other scripts to run
- Adjusted all scripts across the board to support both dnf and apt package managers
- Added the Server Password variable for template `.ini`
- Updated server-reboot to save again within the last 5 seconds
- Updated READEME.md slightly to reflect new changes
- Updated auto-cron.sh to save your current cron settings + add in comments. (will have it append next time instead)

### Fixed
- Fixed a Bad Hardcoded path in `initial-start.sh` (bad on me smh)

### Removed
- install-steam.sh

### Tests
- Tested on Ubuntu 22.04
- Tested on Centos Stream 9
