#!/bin/bash

# shellcheck disable=SC1091
source "$(dirname "$0")/config.sh"


# Get the absolute path to the directory containing the script
# shellcheck disable=SC2128
script_dir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)

# Set the relative path to the backup directory
# shellcheck disable=SC2154
backup_dir="$homedir/PalWorld-Backups"

# Directory to store backups
# Check if the backup directory already exists
if [ -d "$backup_dir" ]; then
    echo "Backup directory $backup_dir already exists. Skipping Creation of new Directory"
else
    # Create the backup directory
    mkdir "$backup_dir"
    echo "Backup directory $backup_dir created."
fi

# Backup the existing crontab
crontab -l > "$backup_dir/crontab_backup_$(date +"%Y%m%d_%H%M%S").txt"

# Define the new crontab entries with the full path
cron_entries=(
    "# Restarts the server every 5 minutes"
    "*/5 * * * * $script_dir/server-check.sh"

    "# Backs up Server Data every 3 hours"
    "0 */3 * * * $script_dir/backup.sh"
    
    "# Updates the server at 4 AM daily"
    "0 4 * * * $script_dir/update.sh"

    "# Reboots the server at 5:00 AM and 5:00 PM daily"
    "0 5 * * * $script_dir/server-reboot.sh"
    "0 17 * * * $script_dir/server-reboot.sh"
)

# Combine the cron entries into a single string
cron_string=$(IFS=$'\n'; echo "${cron_entries[*]}")

# Load the new crontab entries
echo "$cron_string" | crontab - || { echo "Failed to load crontab entries."; exit 1; }

echo "Crontab entries added successfully. Backup created in $backup_dir."
