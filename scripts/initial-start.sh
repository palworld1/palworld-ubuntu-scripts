#!/bin/bash

# shellcheck disable=SC1091
source "$(dirname "$0")/config.sh"

# shellcheck disable=SC2128
script_dir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)

# Set steam directory
# shellcheck disable=SC2154
steamdir="$homedir/Steam/steamapps/common/PalServer"

# Display the current working directory
echo -e "Current working directory: $script_dir \n"

# Navigate to Pal Server location
cd "$steamdir" || exit

echo -e "Starting server to generate files then stopping\n"

screen -dmS palworld "$steamdir/PalServer.sh" "$PAL_SERVER_ARGS"

echo -e "Waiting for server to start and stop \n"
# Wait for 5 seconds
sleep 5

# While the screen is still up
# Enter a control+x to close the server
# Check to make sure it's not up every 10 seconds.
while screen -list | grep -q "palworld"; do
    echo -e "Screen session 'palworld' is currently running. \n"
    screen -S palworld -X stuff $'\003'
    sleep 10
done

echo -e "Screen session 'palworld' is not running. Continuing with Script\n"

configpath="$homedir/Steam/steamapps/common/PalServer/Pal/Saved/Config/LinuxServer"

# Specify the template file
template_file="$script_dir/PalWorldSettings_template.ini"

# Specify the destination file
destination_file="$configpath/PalWorldSettings.ini"

# Define environment variables
# Replace these with your own values; these are merely examples
# I need to find a better way to do this.... (python?)
ADMIN_PASS="test12345"
SERVER_DESC="My PalWorld Server!"
SERVER_NAME="PalWorld-Fun"
SERVER_PASS="czSKD834f982Kf9"
PORT=8211

# Export vars for templating
export ADMIN_PASS="$ADMIN_PASS"
export SERVER_DESC="$SERVER_DESC"
export SERVER_NAME="$SERVER_NAME"
export SERVER_PASS="$SERVER_PASS"
export PORT="$PORT"

# Ensure the template file exists
if [ -f "$template_file" ]; then
    # Use envsubst to substitute variables and create the destination file
    envsubst < "$template_file" > "$destination_file"
    echo -e "Settings have been written to $destination_file\n"
else
    echo "Error: Template file $template_file not found."
    exit 1
fi

cat "$destination_file"
