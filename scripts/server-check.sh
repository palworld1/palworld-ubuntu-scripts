#!/bin/bash

# shellcheck disable=SC1091
source "$(dirname "$0")/config.sh"

# shellcheck disable=SC2154
steamdir="$homedir/Steam/steamapps/common/PalServer"
PAL_SERVER_PATH="$steamdir/PalServer.sh"

ARRCON_PATH="$homedir/ARRCON"


# Function to check total memory usage percentage
# Function to check total memory usage percentage
check_memory_usage() {
    free -m | awk '/^Mem:/ {printf "%.2f", ($3 / $2) * 100}'
}


# Function to execute ARRCON commands
execute_arrcon_commands() {
    $ARRCON_PATH -H "$RCON_HOST" -P "$RCON_PORT" -p "$RCON_PASSWORD" <<EOF
$(printf "%s\n" "${@}")
EOF
}

# Function to check if the Palworld screen is running
is_palworld_running() {
    screen -list | grep -q "palworld"
}

#!/bin/bash

# Check if update screen is running
if screen -list | grep -q "palworld-update"; then
    echo "Update screen is already running. Waiting for 30 seconds..."
    sleep 30
else
    echo "Update not running. Continuing"
fi


# Check if palworld screen is running
if is_palworld_running; then
    echo "palworld screen is already running."
else
    # If not running, start PalServer.sh
    echo "palworld screen is not running. Starting PalServer.sh..."
    screen -dmS palworld "$PAL_SERVER_PATH" "$PAL_SERVER_ARGS"

    # Wait for a few seconds to give the process time to start
    sleep 5

    # Check if the process has started
    if is_palworld_running; then
        echo "PalServer.sh started successfully in a new screen session."
    else
        echo "Failed to start PalServer.sh. Check logs or configuration."
        exit 1  # Exit with an error code
    fi
fi

# Check memory usage and execute ARRCON commands if over threshold
memory_percentage=$(check_memory_usage)
memory_percentage_numeric=$(echo "$memory_percentage" | awk '{print int($1)}')

# Use bash arithmetic for comparison
if (( memory_percentage_numeric > RAM_RESTART )); then
    echo "Memory usage exceeds threshold. Sending ARRCON commands..."
    execute_arrcon_commands "Save" "Shutdown 60" "Broadcast The-System-Has-Exceeded-80%-Ram-Usage-Will-Reboot-In-1-Min"

    # Wait for the server to shut down (60 seconds) and restart (additional time if needed)
    sleep 70

    # Check if the PalServer process is still running after restart
    if is_palworld_running; then
        echo "PalServer.sh is still running after restart."
    else
        echo "PalServer.sh has Stopped. Check logs or configuration."
        echo "Reboot will trigger within the next cron window"    
    fi
else
    echo "Memory usage is below 80%. Current usage: $memory_percentage%"
    echo "Restart is not needed at this time"
fi
