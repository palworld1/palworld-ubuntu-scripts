#!/bin/bash

# shellcheck disable=SC1091
source "$(dirname "$0")/config.sh"

homedir=$HOME
# Path to the ARRCON script
ARRCON_PATH="$homedir/ARRCON"

# Commands to execute
COMMANDS=(
    "Save"
    "Shutdown 180"
)

# Execute ARRCON with RCON commands
$ARRCON_PATH -H "$RCON_HOST" -P "$RCON_PORT" -p "$RCON_PASSWORD" <<EOF
$(printf "%s\n" "${COMMANDS[@]}")
EOF

# Sleep for 59 Seconds
sleep 59

# Execute ARRCON with RCON commands
$ARRCON_PATH -H "$RCON_HOST" -P "$RCON_PORT" -p "$RCON_PASSWORD" <<EOF
Broadcast 2min-Remaining
EOF

# Sleep for 59 Seconds
sleep 59

# Execute ARRCON with RCON commands
$ARRCON_PATH -H "$RCON_HOST" -P "$RCON_PORT" -p "$RCON_PASSWORD" <<EOF
Broadcast 1min-Remaining
EOF

# Sleep for 29 Seconds
sleep 29

# Execute ARRCON with RCON commands
$ARRCON_PATH -H "$RCON_HOST" -P "$RCON_PORT" -p "$RCON_PASSWORD" <<EOF
Broadcast 30sec-Remaining
EOF

# Sleep for 19 Seconds
sleep 19

# Execute ARRCON with RCON commands
$ARRCON_PATH -H "$RCON_HOST" -P "$RCON_PORT" -p "$RCON_PASSWORD" <<EOF
Broadcast 10sec-Remaining
EOF

# Sleep for 5 Seconds
sleep 4

# Execute ARRCON with RCON commands
$ARRCON_PATH -H "$RCON_HOST" -P "$RCON_PORT" -p "$RCON_PASSWORD" <<EOF
Save
EOF

# Execute ARRCON with RCON commands
$ARRCON_PATH -H "$RCON_HOST" -P "$RCON_PORT" -p "$RCON_PASSWORD" <<EOF
Broadcast 5sec-Remaining
EOF

# Sleep for 2 Seconds
sleep 2

# Execute ARRCON with RCON commands
$ARRCON_PATH -H "$RCON_HOST" -P "$RCON_PORT" -p "$RCON_PASSWORD" <<EOF
Broadcast Server-Will-Begin-Closing
EOF
