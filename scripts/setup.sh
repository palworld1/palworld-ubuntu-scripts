#!/bin/bash

# shellcheck disable=SC1091
source "$(dirname "$0")/config.sh"

# Check if the script is run as root
if [ "$(id -u)" -eq 0 ]; then
    echo "This script must NOT be run as root. Please run it as a regular user."
    exit 1
fi

# Get the current working directory and save it as a variable

# Function to update the system time based on the selected timezone
update_timezone() {
    sudo timedatectl set-timezone "$1"
    echo "System time updated to $1."
}

# Check the distribution
if [ -f /etc/debian_version ]; then
    PACKAGE_MANAGER="apt"
elif [ -f /etc/redhat-release ]; then
    PACKAGE_MANAGER="dnf"
else
    echo "Unsupported distribution. Exiting."
    exit 1
fi

# Options for the user to choose from
timezones=("America/New_York" "America/Chicago" "America/Denver" "America/Los_Angeles" "America/Anchorage" "America/Honolulu"
           "Europe/London" "Europe/Paris" "Europe/Berlin" "Europe/Rome" "Europe/Madrid" "Europe/Istanbul")

# Display menu
echo "Select a timezone to update the system time:"
select timezone in "${timezones[@]}"; do
    if [ "$timezone" ]; then
        update_timezone "$timezone"
        break
    else
        echo "Invalid option. Please select a valid number."
    fi
done

# Use the determined package manager to install wget, unzip, and screen
if [ "$PACKAGE_MANAGER" == "apt" ]; then
    sudo $PACKAGE_MANAGER install wget unzip screen htop -y
elif [ "$PACKAGE_MANAGER" == "dnf" ]; then
    # Install EPEL repository for CentOS
    if [ "$PACKAGE_MANAGER" == "dnf" ]; then
        sudo $PACKAGE_MANAGER install epel-release -y
        sudo $PACKAGE_MANAGER install screen unzip wget htop procps-ng -y
    fi
fi

# Check if ARRCON-3.3.7-Linux.zip already exists
# shellcheck disable=SC2154
if [ -e "$homedir/ARRCON-3.3.7-Linux.zip" ]; then
    echo "ARRCON-3.3.7-Linux.zip already exists. Skipping download."
else
    # Download ARRCON
    wget https://github.com/radj307/ARRCON/releases/download/3.3.7/ARRCON-3.3.7-Linux.zip -P "$homedir"
    unzip "$homedir/ARRCON-3.3.7-Linux.zip" -d "$homedir"
fi

echo -e "\nPre-Steam Set Up Complete\n"

# Download and install SteamCMD
if [ "$PACKAGE_MANAGER" == "apt" ]; then
    sudo add-apt-repository multiverse; sudo dpkg --add-architecture i386; sudo apt update
    sudo $PACKAGE_MANAGER install steamcmd -y
    # Install Palworld server using SteamCMD
    echo -e "\n Steamcmd Installed and Updated\n"
    mkdir -p ~/.steam/sdk64/
    steamcmd +login anonymous +app_update 1007 +quit
    echo -e "\n Steam Redistributables Installed\n"
    cp ~/Steam/steamapps/common/Steamworks\ SDK\ Redist/linux64/steamclient.so ~/.steam/sdk64/
    steamcmd +login anonymous +app_update 2394010 validate +quit
elif [ "$PACKAGE_MANAGER" == "dnf" ]; then
    # Function to check if a file exists in a directory
    file_exists() {
        [ -e "$1/$2" ]
    }

    # Check if steamcmd_linux.tar.gz already exists in ~/Steam
    if file_exists ~/Steam "steamcmd_linux.tar.gz"; then
        echo "steamcmd_linux.tar.gz already exists. Skipping download."
        cd ~/Steam || exit
        ./steamcmd.sh +login anonymous +app_update 2394010 validate +quit
    else
        # Download steamcmd_linux.tar.gz
        mkdir -p ~/Steam
        cd ~/Steam || exit
        wget https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz
        sudo $PACKAGE_MANAGER install wget tar glibc.i686 libstdc++.i686 -y
        $PACKAGE_MANAGER --enablerepo=powertools install SDL2.i686 -y
        mkdir ~/Steam
        cd ~/Steam || exit
        wget https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz
        tar -xzvf steamcmd_linux.tar.gz
        ./steamcmd.sh +quit
        echo -e "\n Steamcmd Installed and Updated\n"
        mkdir -p ~/.steam/sdk64/
        ./steamcmd.sh +login anonymous +app_update 1007 +quit
        echo -e "\n Steam Redistributables Installed\n"
        cp ~/Steam/steamapps/common/Steamworks\ SDK\ Redist/linux64/steamclient.so ~/.steam/sdk64/
        ./steamcmd.sh +login anonymous +app_update 2394010 validate +quit
    fi

fi

echo -e "\nPalworld server has been installed successfully!\n"
