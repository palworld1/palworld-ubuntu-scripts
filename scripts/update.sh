#!/bin/bash

# shellcheck disable=SC1091
source "$(dirname "$0")/config.sh"

# shellcheck disable=SC2154
ARRCON_PATH="$homedir/ARRCON"

# Function to execute ARRCON commands
execute_arrcon_commands() {
    $ARRCON_PATH -H "$RCON_HOST" -P "$RCON_PORT" -p "$RCON_PASSWORD" <<EOF
$(printf "%s\n" "${@}")
EOF
}

# Function to check if the Palworld screen is running
is_palworld_running() {
    screen -list | grep -q "palworld"
}

# Check the distribution
if [ -f /etc/debian_version ]; then
    PACKAGE_MANAGER="apt"
elif [ -f /etc/redhat-release ]; then
    PACKAGE_MANAGER="dnf"
else
    echo "Unsupported distribution. Exiting."
    exit 1
fi

# Check if palworld screen is running
if screen -ls | grep -q "palworld"; then
    echo "palworld screen is already running. Please shut down the server."
    execute_arrcon_commands "Save" "Shutdown 60" "Broadcast The-System-Will-Check-For-Updates-And-Reboot-In-1-Min"

    # Wait for the server to shut down (60 seconds) and restart (additional time if needed)
    sleep 70

    # Check if the PalServer process is still running after restart
    if is_palworld_running; then
        echo "PalServer.sh is still running after restart."
    else
        echo "PalServer.sh has stopped. Continuing with Update."
    fi
    # If not running, start the screen session
    echo "palworld screen is not running. Starting Update session."

    # Create a new "palworld-update" screen
    screen -dmS palworld-update

    # Check the package manager and send appropriate command
    if [ "$PACKAGE_MANAGER" == "apt" ]; then
        # Send the command to the "palworld-update" screen with an additional newline
        screen -S palworld-update -X stuff "steamcmd +login anonymous +app_update 2394010 validate +quit^M"
    elif [ "$PACKAGE_MANAGER" == "dnf" ]; then
        # Send the command to the "palworld-update" screen with an additional newline
        screen -S palworld-update -X stuff "cd $HOME/Steam && ./steamcmd.sh +login anonymous +app_update 2394010 validate +quit^M"
    else
        echo "Unsupported package manager. Exiting."
        exit 1
    fi

    # wait 30 seconds
    sleep 30
    # Detach from the "palworld" screen
    screen -S palworld-update -X quit

else
    # If not running, start the screen session
    echo "palworld screen is not running. Starting Update session."

    # Create a new "palworld-update" screen
    screen -dmS palworld-update

    # Check the package manager and send appropriate command
    if [ "$PACKAGE_MANAGER" == "apt" ]; then
        # Send the command to the "palworld-update" screen with an additional newline
        screen -S palworld-update -X stuff "steamcmd +login anonymous +app_update 2394010 validate +quit^M"
    elif [ "$PACKAGE_MANAGER" == "dnf" ]; then
        # Send the command to the "palworld-update" screen with an additional newline
        screen -S palworld-update -X stuff "cd $HOME/Steam && ./steamcmd.sh +login anonymous +app_update 2394010 validate +quit^M"
    else
        echo "Unsupported package manager. Exiting."
        exit 1
    fi

    # wait 30 seconds
    sleep 30
    # Detach from the "palworld" screen
    screen -S palworld-update -X quit
fi
