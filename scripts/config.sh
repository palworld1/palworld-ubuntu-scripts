#!/bin/bash
# This script is used to configure and manage a PalServer scripts.
# shellcheck disable=SC2034
# Disable certain ShellCheck warnings
# SC2034: Unused variables (homedir is used later in downstream files)
# SC1091: Can't follow non-constant source
# Note: This disables a warning about the source command that is used to include configuration files.

# Define the home directory
homedir=$HOME

# RCON connection details
RCON_HOST="127.0.0.1"                # IP address for RCON connection
RCON_PORT="25575"                    # Port for RCON connection
RCON_PASSWORD="YOUR_ADMIN_PASSWORD"  # Admin password for RCON connection

# PalServer details
# Path to PalServer.sh
PAL_SERVER_PATH="$HOME/Steam/steamapps/common/PalServer/PalServer.sh" 

# Arguments for launching PalServer.sh with specific configuration
PAL_SERVER_ARGS="-port=8211 -players=32 -useperfthreads -NoAsyncLoadingThread -UseMultithreadForDS"

# Memory usage threshold
# Threshold percentage for restarting the server due to high memory usage
RAM_RESTART=80
