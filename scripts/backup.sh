#!/bin/bash

# shellcheck disable=SC1091
source "$(dirname "$0")/config.sh"

# Specify the path
# shellcheck disable=SC2154
save_path="$homedir/Steam/steamapps/common/PalServer/Pal/Saved/SaveGames/0/"

# Change to the specified directory
cd "$save_path" || exit

# Get the first subdirectory inside "SaveGames/0/"
custom_path_variable=""
for dir in */; do
    custom_path_variable="$dir"
    break
done

# find the path
if [ -n "$custom_path_variable" ]; then
    full_path="${save_path}${custom_path_variable}"
else
    echo "No subdirectories found in the specified path."
fi

# Set the relative path to the backup directory
backup_dir="$homedir/PalWorld-Backups"

# Check if the backup directory already exists
if [ -d "$backup_dir" ]; then
    echo "Backup directory $backup_dir already exists. Skipping Creation of new Directory"
else
    # Create the backup directory
    mkdir "$backup_dir"
    echo "Backup directory $backup_dir created."
fi

# Additional files to include in the backup
file2="Level.sav"
file3="LevelMeta.sav"

# Create a timestamp for the backup file
timestamp=$(date +"%-m-%-d-%y-%-I-%M-%p")
backup_filename="palworld-backup-$timestamp.tar"

# Expand the ~ symbol to the home directory in the backup_dir
backup_dir=$(eval echo "$backup_dir")

# Create the destination directory if it doesn't exist
mkdir -p "$backup_dir"

# Change to the source directory
cd "$full_path" || exit

# Create the tar archive with the contents of the Players directory
tar -cf "$backup_dir/$backup_filename" Players "$file2" "$file3"

echo "Backup completed at $timestamp"

# Clean Up

# Find and delete files older than the 3 days
find "$backup_dir" -name "palworld-backup-*.tar" -type f -mtime +3 -exec rm {} \;

echo -e "\nCleanup run. Backups over 3 days old were deleted."