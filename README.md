# Palworld-Linux-Scripts

## Getting started
To use these scripts either git clone down this repo or copy them manually into your desired folder path.

There is a general assumption you know basic linux commands when following this doc. (I'll try to be explicit)

**THIS GUIDE DOES NOT COVER PORT FORWARDING**
- It is assumed you already have the ports you need + an internet connection available

**THIS GUIDE DOES NOT COVER RESTORING FROM BACKUP SAVES**

- Yet :) 
- It will **NOT** cover using another tool to edit saves. Please look at those tools instead :)

## Description
This project is a bunch of BASH scripts written to help manage linux PalWorld Servers. I personally use these scripts to personally manage three Ubuntu-22.04 servers and thought that the community might be interested in the capabilities as well. The thought process behind all BASH was so that I didn't need python or other packages not available in a non-updated package-manager to start and play.

### Features:
- Auto Update at 4am every day
- Auto Backup of the entire `/Pal` directory
- Auto Restart on crash or exceeding 80% memory usage 
- Automated install for SteamCMD, Steamworks SDK Redist, and PalWorld Server Binaries.
- Easy basic template for PalWorldSettings.ini server options

## Disclaimer
- These scripts are designed to be run in order and may or may not function fully independently of one another.

- These scripts may also not function without the pre-requisites installed or when run against server instances not setup with these scripts.
(I'm only human after all)

- These scripts do not send or collect any data outside of what SteamCMD and PalWorld Crash Reports / Epic Server pings collect.

- This does **NOT** use any PalWorld Save editing tools or install any alternate binaries that have been floating around. Official Stuff Only!

- You are responsible for securing your server's Admin Password & Server Password. Please choose a complexity compliant password!

## Pre-Requisites:

- An account that has `sudo` access. You should not run these scripts as the `root` user.
- An Ubuntu-22.04 or Centos Stream 9 linux operating system.
- Binaries
    - screen
    - htop
    - wget
    - unzip


## Installation
See the installation guide here! (Comes with pictures!)

[install-guide](docs/install-guide.md)


## Support
Open an Issue and I'll try to help where I can. If not I recommend the r/Palworld Discord or Reddit in general. 

You can also try ChatGPT which may help since its probably better at bash programming than me!

## Roadmap
-   Instructions for root users to make a non-root user
-   A video install guide
-   A wrapper script that goes end to end
-   Better code quality, linting, and scanning
-   Actually updating these scripts
-   Integrate a discord bot

## Contributing

Feel free to submit contributions and recommendations. All I ask is the following:

1. Comment your code
2. Use common sense
3. Pass the linter (feel free to implement it before I do. I use shellcheck locally atm)


## Authors and acknowledgment
- KengTeong

    
### Remote connection tool:

- https://github.com/radj307/ARRCON

## License
For the PalWorld community to use for Linux Server operation.

If you fork, clone, or otherwise distribute this, please be sure to give credit! 

## Project status
Side-project for fun. Currently used by developer.

## Accomplished objectives
- Centos compatible 
- "Better" install Documentation